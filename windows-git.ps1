
$LOGFILE = "c:\logs\windows-git.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

  Write-Log "Start windows-git"
  try {
        $ProgressPreference = 'SilentlyContinue'
        # get latest download url for git-for-windows 64-bit exe
        $git_url = "https://api.github.com/repos/git-for-windows/git/releases/latest"
        $asset = Invoke-RestMethod -Method Get -Uri $git_url | % assets | where name -like "*64-bit.exe"
        # download installer
        $installer = "$env:temp\$($asset.name)"
        Invoke-WebRequest -Uri $asset.browser_download_url -OutFile $installer
        # run installer
        $git_install_inf = "<install inf file>"
        $install_args = "/SP- /VERYSILENT /SUPPRESSMSGBOXES /NOCANCEL /NORESTART /CLOSEAPPLICATIONS /RESTARTAPPLICATIONS /LOADINF=""$git_install_inf"""
        Start-Process -FilePath $installer -ArgumentList $install_args -Wait

   #     $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
 }
  catch {
      Write-Log "$_"
      Throw $_
  }
  

  Write-Log "End  windows-git"
 
}

Main 

